import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'database.dart';
import 'medicine_listing.dart';
import 'package:gtext/gtext.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'app_State.dart';


class AddMedicinePage extends StatefulWidget {
  AddMedicinePage({super.key, required this.medicine});

  MedicineListing medicine;

  @override
  _AddMedicinePageState createState() => _AddMedicinePageState();
}

class _AddMedicinePageState extends State<AddMedicinePage> {
  TextEditingController medicineNameController = TextEditingController();
  String selectedFrequency = "Daily";
  List<TimeOfDay> selectedTimes = [TimeOfDay.now()];
  List<bool> selectedDays = List.generate(7, (index) => false);
  final _timeControllers = [TextEditingController()];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      medicineNameController.text = widget.medicine.brandName!;
    });
  }

  @override
  Widget build(BuildContext context) {
    print(App_State.locale.value);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromRGBO(226, 252, 214, 1.0),
        toolbarHeight: 80,
        flexibleSpace: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 65,
                width: 150,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/logo.png'),
                      fit: BoxFit.fitHeight),
                ),
              ),
              GText(

                  'Add Medicine',
                  toLang: App_State.locale.value == 'zh' ? 'zh-cn' : 'en',
                  style: TextStyle(
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 25,
                      color: Color.fromRGBO(9, 93, 126, 1.0)))
            ],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: medicineNameController,
                decoration: InputDecoration(
                  labelText: 'Medicine Name',
                  hintText: 'Medicine Name',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              GText('Frequency:', toLang: App_State.locale.value == 'zh' ? 'zh-cn' : 'en',),
              Align(
                alignment: Alignment.center,
                child: DropdownButton<String>(
                  value: selectedFrequency,
                  onChanged: (value) {
                    setState(() {
                      selectedFrequency = value!;
                      if (selectedFrequency == 'Weekly') {
                        selectedDays = List.generate(7, (index) => false);
                      }
                    });
                  },
                  items: ['Daily', 'Weekly'].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: GText(value, toLang: App_State.locale.value == 'zh' ? 'zh-cn' : 'en',),
                    );
                  }).toList(),
                ),
              ),
              if (selectedFrequency == 'Weekly') ...[
                const SizedBox(height: 20),
                 GText('Select Days:', toLang: App_State.locale.value == 'zh' ? 'zh-cn' : 'en',),
                Wrap(
                  children: List.generate(7, (index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ChoiceChip(
                        label: Text(_getDayName(index),
                            style: TextStyle(color: Colors.white)),
                        selected: selectedDays[index],
                        selectedColor: Color.fromRGBO(9, 93, 126, 1.0),
                        backgroundColor: Color.fromRGBO(72, 88, 94, 1.0),
                        onSelected: (selected) {
                          setState(() {
                            selectedDays[index] = selected;
                          });
                        },
                      ),
                    );
                  }),
                ),
              ],
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GText('Time:', toLang: App_State.locale.value == 'zh' ? 'zh-cn' : 'en',),
                  IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      setState(() {
                        _timeControllers.add(TextEditingController());
                        selectedTimes.add(TimeOfDay.now());
                      });
                    },
                  ),
                ],
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: _buildTimeFields(),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Align(
                alignment: Alignment.center,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Color.fromRGBO(9, 93, 126, 1.0)),
                  onPressed: () {
                    _addMedicineDetails();
                  },
                  child: GText('Save', toLang: App_State.locale.value == 'zh' ? 'zh-cn' : 'en',),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _getDayName(int index) {
    final days = [AppLocalizations.of(context)?.monday ?? "", AppLocalizations.of(context)?.tuesday ?? "", AppLocalizations.of(context)?.wednesday ?? "", AppLocalizations.of(context)?.thursday ?? "", AppLocalizations.of(context)?.friday ?? "", AppLocalizations.of(context)?.saturday ?? "", AppLocalizations.of(context)?.sunday ?? ""];
    return days[index];
  }

  List<Widget> _buildTimeFields() {
    List<Widget> timeFields = [];

    for (int i = 0; i < selectedTimes.length; i++) {
      timeFields.add(
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                flex: 7,
                child: TextField(
                  keyboardType: TextInputType.text,
                  enabled: false,
                  decoration: InputDecoration(
                    labelText: 'Time',
                    hintText: 'Time',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                  controller: _timeControllers[i],
                ),
              ),
              Expanded(
                child: IconButton(
                  icon: Icon(Icons.access_time),
                  onPressed: () async {
                    TimeOfDay? pickedTime = await showTimePicker(
                      context: context,
                      initialTime: selectedTimes[i],
                    );

                    if (pickedTime != null && pickedTime != selectedTimes[i]) {
                      setState(() {
                        selectedTimes[i] = pickedTime;
                        _timeControllers[i].text =
                            selectedTimes[i].format(context);
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }

    return timeFields;
  }

  snapBarBuilder(String message) {
    final snackBar = SnackBar(
      content: GText(message, toLang: App_State.locale.value == 'zh' ? 'zh-cn' : 'en',),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _addMedicineDetails() {
    print('Medicine Name: ${medicineNameController.text}');
    print('Frequency: $selectedFrequency');
    if (selectedFrequency == 'Daily') {
      selectedDays = List.generate(7, (index) => true);
    }
    print(selectedDays);
    List<String> times = [];
    for (int i = 0; i < selectedTimes.length; i++) {
      print('Time ${i + 1}: ${selectedTimes[i].format(context)}');
      times.add(selectedTimes[i].format(context));
    }
    widget.medicine.setFrequency(selectedDays);
    widget.medicine.setTime(times);
    Database.saveMeds(widget.medicine)
        .then((value) => snapBarBuilder('Medicine detail is saved'));
  }
}
