import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'medicine_listing.dart';
import 'user.dart';

class Database {
  static final db = FirebaseFirestore.instance;
  static final storageRef = FirebaseStorage.instance.ref();

  static Future<bool> addUser(UserStuff user) async {
    await db.collection("users").add(user.convertMap());

    return true;
  }

  static Future<List<UserStuff>> findAge(int wantAge) async {
    final userBelowThirty =
        await db.collection("users").where("age", isLessThan: wantAge).get();
    final userBelowThirtyDocs = userBelowThirty.docs;

    List<Map<String, dynamic>> userinfo = [];
    List<UserStuff> users = [];

    for (var docSnapshot in userBelowThirtyDocs) {
      userinfo.add(docSnapshot.data());
    }

    for (int i = 0; i < userinfo.length; i++) {
      users.add(UserStuff(
          email: userinfo[i]["email"],
          firstName: userinfo[i]["firstName"],
          lastName: userinfo[i]["lastName"],
          age: userinfo[i]["age"],
          meds: userinfo[i]["medicines"]
      ));
    }

    return users;
  }

  static void delete(String ID) async {
    await db.collection("users").doc(ID).delete();
  }

  static void update(String ID, UserStuff user) async {
    final reference = db.collection("users").doc(ID);

    try {
      await reference.update(user.convertMap());
    } catch (e) {
      print(e);
    }
    //await reference.update(user.convertMap());
  }

  static Future<UserStuff> getUser(String email) async {
    final userBelowThirty =
        await db.collection("users").where("email", isEqualTo: email).get();
    final userBelowThirtyDocs = userBelowThirty.docs;

    List<Map<String, dynamic>> userinfo = [];
    List<UserStuff> users = [];

    for (var docSnapshot in userBelowThirtyDocs) {
      userinfo.add(docSnapshot.data());
    }

    for (int i = 0; i < userinfo.length; i++) {
      users.add(UserStuff(
          email: userinfo[i]["email"],
          firstName: userinfo[i]["firstName"],
          lastName: userinfo[i]["lastName"],
          age: userinfo[i]["age"],
          meds: getMedicineList(userinfo[i]['medicines'])));
    }

    return users.first;
  }

  static List<MedicineListing> getMedicineList(List medicines) {
    List<MedicineListing> medicineList = [];
    if (medicines != null) {
      for (int i = 0; i < medicines.length; i++) {
        medicineList.add(MedicineListing(
            brandName: medicines[i]["brandName"],
            purpose: medicines[i]["purpose"],
            warnings: medicines[i]["warnings"],
            directions: medicines[i]["directions"],
            route: medicines[i]["route"],
            ingredients: medicines[i]["ingredient"],
            frequency: medicines[i]['frequency'],
            time: medicines[i]['time']));
        print(medicineList[i].brandName);
      }
    }
    return medicineList;
  }

  static Future<bool> storeImage(File image, String userName) async {
    final uploadRef = storageRef.child(userName);

    try {
      await uploadRef.putFile(image);
    } catch (e) {
      return false;
    }

    return true;
  }

  static Future<List> getMeds() async {
    var user = FirebaseAuth.instance.currentUser;
    List medicines = [];
    var name = await db
        .collection("users")
        .where("email", isEqualTo: user?.email)
        .get();
    if (name.docs[0].data()['medicines'] != null) {
      medicines = name.docs[0].data()['medicines'];
    }

    return medicines;
  }

  static Future<bool> saveMeds(MedicineListing med) async {
    var user = FirebaseAuth.instance.currentUser;
    var name = await db
        .collection("users")
        .where("email", isEqualTo: user?.email)
        .get();
    String id = name.docs[0].id;

    List medicine = await getMeds();
    medicine.add(med.getInformation());
    db
        .collection('users')
        .doc(id)
        .set({'medicines': medicine}, SetOptions(merge: true));

    return true;
  }

  static Future<bool> updateMeds(MedicineListing med, int index) async {
    var user = FirebaseAuth.instance.currentUser;
    var name = await db
        .collection("users")
        .where("email", isEqualTo: user?.email)
        .get();
    String id = name.docs[0].id;

    List medicine = await getMeds();
    medicine[index] = med.getInformation();
    db
        .collection('users')
        .doc(id)
        .set({'medicines': medicine}, SetOptions(merge: true));

    return true;
  }

  /*this.brandName,
    this.purpose,
    this.warnings,
    this.directions,
    this.route,
    this.ingredients,
    this.frequency,
    this.time, */

  static Future<MedicineListing?> getCachedMedTranslation(String name) async {
    var meds = await db
      .collection('cachedMedTranslations')
      .where('brandName', isEqualTo: name)
      .get();
    if (meds.docs.firstOrNull?.data()["englishBrandName"] == name) {
      return MedicineListing(
        brandName: meds.docs.firstOrNull!.data()["brandName"],
        purpose: meds.docs.firstOrNull!.data()["purpose"],
        warnings: meds.docs.firstOrNull!.data()["warnings"],
        directions: meds.docs.firstOrNull!.data()["directions"],
        route: meds.docs.firstOrNull!.data()["route"],
        ingredients: meds.docs.firstOrNull!.data()["ingredients"],
        frequency: meds.docs.firstOrNull!.data()["frequency"],
        time: meds.docs.firstOrNull!.data()["time"]
      );
    }
    return null;
  }

  static Future<void> addCachedTranslation(MedicineListing listing) async {
    var meds = await db
      .collection('cachedMedTranslations')
      .add(listing.getInformation());
  }

}
