import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'themeProvider.dart';
import 'package:provider/provider.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app_State.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DetailsPage extends StatefulWidget {
  const DetailsPage({Key? key}) : super(key: key);

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SettingsList(
        sections: [
          SettingsSection(
            title: Text(
              AppLocalizations.of(context)?.settings ?? "",
              style: TextStyle(
                fontSize: 30,
                color: Provider.of<ThemeProvider>(context).isDarkModeOn
                    ? Colors.white
                    : Colors.black,
              ),
            ),
            tiles: <SettingsTile>[
              SettingsTile.switchTile(
                onToggle: (value) {
                  Provider.of<ThemeProvider>(context, listen: false)
                      .toggleTheme();
                },
                initialValue: Provider.of<ThemeProvider>(context).isDarkModeOn,
                leading: const Icon(Icons.format_paint),
                title: Text(AppLocalizations.of(context)?.darkmode ?? ""),
              ),

              SettingsTile(
                title: Text(AppLocalizations.of(context)?.language ?? ""),
                onPressed: (BuildContext context){SharedPreferences.getInstance().then((value) {
                  if (value.getString("locale") == "en") {
                    App_State.locale.value = "zh";
                    print("changed!!!");
                    value.setString("locale", "zh");
                  } else {
                    App_State.locale.value = "en";
                    value.setString("locale", "en");
                  }
                });}),

              SettingsTile.navigation(
                title: Text(AppLocalizations.of(context)?.signout ?? ""),
                leading: const Icon(Icons.exit_to_app),
                trailing: const Icon(Icons.arrow_forward_ios_sharp),
                onPressed: (BuildContext context) {
                  _signOut(context);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  // Function to handle user logout
  void _signOut(BuildContext context) async {
    try {
      await FirebaseAuth.instance.signOut();
      // Add any additional logic you need after logout
      // For example, you may want to navigate to the login screen.
      Navigator.popUntil(
          context, ModalRoute.withName(Navigator.defaultRouteName));
    } catch (e) {
      print('Error during logout: $e');
    }
  }
}
