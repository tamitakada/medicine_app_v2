import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'route.dart';

import 'database.dart';
import 'user.dart';

class UserInformation extends StatefulWidget {
  final String email;

  const UserInformation({super.key, required this.email});

  @override
  State<UserInformation> createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  final ImagePicker imagePicker = ImagePicker();
  File? selectedImage;

  ImageFromCamera() async {
    final image = await imagePicker.pickImage(
      source: ImageSource.camera,
      imageQuality: 50,
    );

    Database.storeImage(File(image!.path), widget.email);
  }

  ImageFromGallery() async {
    final image = await imagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 50,
    );

    Database.storeImage(File(image!.path), widget.email);
  }

  void showPicker() {
    //showModalBottomSheet(context: context, builder: builder);
  }

  final formKey = GlobalKey<FormState>();

  TextEditingController firstNameCtrl = TextEditingController();
  TextEditingController lastNameCtrl = TextEditingController();
  TextEditingController ageCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromRGBO(226, 252, 214, 1.0),
        toolbarHeight: 100,
        flexibleSpace: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 65,
                width: 200,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/logo.png'),
                      fit: BoxFit.fitHeight),
                ),
              ),
              const Text('User Information',
                  style: TextStyle(
                    // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 30,
                      color: Color.fromRGBO(9, 93, 126, 1.0)))
            ],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: ListView(
          children: [
            Form(
              key: formKey,
              child: Column(
                children: [
                  const Text(
                    "Input first name",
                    style: TextStyle(fontSize: 20),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.email),
                      labelText: "First Name",
                      border: OutlineInputBorder(),
                    ),
                    controller: firstNameCtrl,
                    validator: (val) {
                      if (val!.isEmpty) {
                        return "Please enter a your first name";
                      }

                      return null;
                    },
                  ),
                  const Text(
                    "Input last name",
                    style: TextStyle(fontSize: 20),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.email),
                      labelText: "Last Name",
                      border: OutlineInputBorder(),
                    ),
                    controller: lastNameCtrl,
                    validator: (val) {
                      if (val!.isEmpty) {
                        return "Please enter your last name";
                      }

                      return null;
                    },
                  ),
                  const Text(
                    "Input age",
                    style: TextStyle(fontSize: 20),
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.email),
                      labelText: "Age",
                      border: OutlineInputBorder(),
                    ),
                    controller: ageCtrl,
                    validator: (val) {
                      if (val!.isEmpty) {
                        return "Please enter your age";
                      }

                      return null;
                    },
                  ),
                  // const Text(
                  //   "Photo of medical ID",
                  //   style: TextStyle(fontSize: 20),
                  // ),
                  // ElevatedButton(
                  //   onPressed: ImageFromGallery,
                  //   child: const Text("Import for photo gallery"),
                  // ),
                  // ElevatedButton(
                  //   onPressed: ImageFromCamera,
                  //   child: const Text("Take a photo"),
                  // ),
                  ElevatedButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        formKey.currentState!.save();

                        Database.addUser(UserStuff(
                            email: widget.email,
                            firstName: firstNameCtrl.text,
                            lastName: lastNameCtrl.text,
                            meds: [],
                            age: int.parse(ageCtrl.text))).then((value) {
                          if (value){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const RoutePage()));
                          }
                        });
                      }
                    },
                    child: const Text("Create"),
                  ),
                ],
              ),
            )
            //timezone?
          ],
        ),
      ),
    );
  }
}
