import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'themeProvider.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';
import 'sign_in.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app_State.dart';
import 'package:gtext/gtext.dart';



Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: "assets/.env");
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  App_State.prefs = await SharedPreferences.getInstance();
  App_State.locale.value = App_State.prefs.getString("locale") ?? "en";

  GText.init(to: App_State.locale.value == 'zh' ? 'zh-cn' : 'en', enableCaching: false);

  runApp(
    ChangeNotifierProvider(
      create: (context) => ThemeProvider(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: App_State.locale,
        builder: (context, _, __) {
          return MaterialApp(
            key: UniqueKey(),
            title: 'To Do',
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            locale: Locale(App_State.locale.value), // Load in actual locale
            theme: ThemeData.light(),
            darkTheme: ThemeData.dark(),
            themeMode: Provider.of<ThemeProvider>(context).isDarkModeOn
                ? ThemeMode.dark
                : ThemeMode.light,
            home: const SignInPage(),
          );
        }
      );
  }
}
