import 'package:flutter/material.dart';
import 'sign_in.dart';
import 'user_information.dart';
import 'route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          const SignUpForm(),
        ],
      ),
    );
  }
}

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  State<SignUpForm> createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final formKey = GlobalKey<FormState>();
  String? email;
  String? password;
  String? confirmPassword;

  bool agreed = false;

  TextEditingController passwordControl = TextEditingController();
  TextEditingController emailControl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Form(
      key: formKey,
      child: Column(
        children: [
          Container(
              color: const Color.fromRGBO(226, 252, 214, 1.0),
              height: 250,
              width: width,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Image.asset(
                  'assets/logo.png',
                ),
              )),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              AppLocalizations.of(context)?.nameOfApp ?? "",
              style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.email),
                labelText: AppLocalizations.of(context)?.email ?? "",
                border: OutlineInputBorder(),
              ),
              controller: emailControl,
              onSaved: (val) {
                email = val;
              },
              validator: (val) {
                if (val!.isEmpty) {
                  return AppLocalizations.of(context)?.emailError ?? "";
                }

                return null;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: passwordControl,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.password),
                labelText: AppLocalizations.of(context)?.password ?? "",
                border: OutlineInputBorder(),
              ),
              onSaved: (val) {
                password = val;
              },
              validator: (val) {
                if (val!.isEmpty) {
                  //"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"

                  return AppLocalizations.of(context)?.passwordError ?? "";
                }

                return null;
              },
              obscureText: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.password),
                labelText: AppLocalizations.of(context)?.reenterPassword ?? "",
                border: OutlineInputBorder(),
              ),
              onSaved: (val) {
                confirmPassword = val;
              },
              validator: (val) {
                if (val!.isEmpty) {
                  //"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"

                  return AppLocalizations.of(context)?.reenterPasswordError ?? "";
                }

                return null;
              },
              obscureText: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Row(
              children: [
                Checkbox(
                  value: agreed,
                  onChanged: (_) {
                    setState(() {
                      agreed = !agreed;
                    });
                  },
                ),
                Expanded(
                  child: Text(
                      AppLocalizations.of(context)?.termsAndPolicy ?? ""),
                ),
              ],
            ),
          ),
          ElevatedButton(
            onPressed: () {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();
                createUser(emailControl.text, passwordControl.text);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => UserInformation(
                              email: emailControl.text,
                            )));
              }
            },
            child: Text(AppLocalizations.of(context)?.signUp ?? ""),
          ),
        ],
      ),
    );
  }

  Future<bool> createUser(String email, String password) async {
    try {
      var user = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      print(e);
      return false;
    }

    return true;
  }
}
