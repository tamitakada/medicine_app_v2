import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'medicineDetailsPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'api_stuff.dart';
import 'medicineListHome.dart';
import 'medicine_listing.dart';
import 'translation.dart';
import 'database.dart';
import 'package:gtext/gtext.dart';
import 'app_State.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? medicine;

  Future<List<MedicineListing>> getMeds(List<MedicineListing> meds) async {
    List<MedicineListing> translated = [];
    for (var med in meds) {
      MedicineListing? newMed = await Database.getCachedMedTranslation(med.brandName ?? "");

      if (newMed != null) {
        translated.add(newMed);
      }
      else {
        MedicineListing newMed2 = await Translation.getMedTranslation(med);
        translated.add(newMed2);
        await Database.addCachedTranslation(newMed2);
      }
    }
    return translated;
  }

  @override
  Widget build(BuildContext context) {


    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromRGBO(226, 252, 214, 1.0),
        toolbarHeight: 100,
        flexibleSpace: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 65,
                width: 65,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/logo.png'),
                      fit: BoxFit.fitHeight),
                ),
              ),
               Text(AppLocalizations.of(context)?.home ?? "",
                  style: TextStyle(
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 30,
                      color: Color.fromRGBO(9, 93, 126, 1.0)))
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.medication_liquid),
                    labelText: AppLocalizations.of(context)?.search ?? "",
                    border: OutlineInputBorder(),
                  ),
                  onSubmitted: (change) {
                    setState(() {
                      medicine = change;
                    });
                  },
                ),
              ),
              FutureBuilder<List<MedicineListing>?>(
                future: apiStuff.find(medicine ?? ""),
                // a previously-obtained Future<String> or null
                builder: (BuildContext context,
                    AsyncSnapshot<List<MedicineListing>?> snapshot) {
                  List<Widget> children;

                  if (snapshot.hasData) {
                    List<MedicineListing> medList = snapshot.data!;

                    return Container(
                      height: height * 0.35,
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          itemCount: snapshot.data?.length ?? 0,
                          itemBuilder: (BuildContext context, int index) {
                            // print(medList[index].brandName);
                            return Container(
                              color: Colors.white60,
                              child: Card(
                                child: ListTile(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              MedicineDetailsScreen(
                                                medicine: medList[index],
                                                isTaking: false,
                                                index:
                                                    0, // This is only for the profile and the reminder
                                              )),
                                    );
                                  },
                                  title: GText(
                                    medList[index].brandName ?? "default value",
                                    toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",
                                  ),
                                  trailing: Icon(Icons.arrow_forward_ios_sharp),
                                ),
                              ),
                            );
                          }),
                    );
                  } else {
                    children = <Widget>[
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: CircularProgressIndicator(),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: Text(AppLocalizations.of(context)?.waiting ?? ""),
                      ),
                    ];
                  }
                  return const Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                  );
                },
              ),
              GText(
                AppLocalizations.of(context)?.reminders ?? "",
                style: TextStyle(fontSize: 40),
                toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",
              ),
              MediciListHome()
            ],
          ),
        ),
      ),
    );
  }
}
