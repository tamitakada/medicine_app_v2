class MedicineListing {
  String? brandName;
  String? purpose;
  List<dynamic>? warnings; //warnings + do_not_use
  String? directions;
  String? route;
  List<dynamic>? ingredients; //active_ingredients + spl_product_data_elements
  List<dynamic>? time = [];
  List<dynamic>? frequency = [];

  MedicineListing({
    this.brandName,
    this.purpose,
    this.warnings,
    this.directions,
    this.route,
    this.ingredients,
    this.frequency,
    this.time,
  });

  void setTime(time) {
    this.time = time;
  }

  void setFrequency(frequency) {
    this.frequency = frequency;
  }

  Map<String, dynamic> getInformation() {
    return {
      'brandName': brandName,
      'purpose': purpose,
      'warnings': warnings,
      'directions': directions,
      'route': route,
      'ingredients': ingredients,
      'frequency': frequency,
      'time': time,
    };
  }
}
