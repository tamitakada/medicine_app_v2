import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'addMedicinePage.dart';
import 'medicineListProfile.dart';
import 'user.dart';
import 'database.dart';
import 'menu.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

//https://open.fda.gov/apis/
//https://api.fda.gov/drug/label.json?search=openfda.generic_name:”ibuprofen”&limit=1

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Widget MedID(double height, double width, UserStuff user) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: height * 0.3,
        width: width* 0.9,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          borderRadius: const BorderRadius.all(Radius.circular(20)),
        ),
        child: MediciListProfile(
          user: user,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //MediaQuery.of(context).size.height or width;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromRGBO(226, 252, 214, 1.0),
        toolbarHeight: 80,
        flexibleSpace: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 65,
                width: 65,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/logo.png'),
                      fit: BoxFit.fitHeight),
                ),
              ),
              Text(AppLocalizations.of(context)?.profile ?? "",
                  style: TextStyle(
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                      fontSize: 25,
                      color: Color.fromRGBO(9, 93, 126, 1.0)))
            ],
          ),
        ),
      ),
      body: FutureBuilder<UserStuff>(
        future: Database.getUser(
            FirebaseAuth.instance.currentUser?.email ?? "Default Value"),
        // a previously-obtained Future<String> or null
        builder: (BuildContext context, AsyncSnapshot<UserStuff> snapshot) {
          print(snapshot);
          List<Widget> children;
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  MedID(height, width,snapshot.data!),
                  Row(
                    children: [
                      Menu(user: snapshot.data!),
                    ],
                  ),
                  // SizedBox(
                  //   width: width * 0.9,
                  //   height: height * 0.1,
                  //   child: OutlinedButton(
                  //     onPressed: null,
                  //     style: OutlinedButton.styleFrom(
                  //       side: const BorderSide(
                  //         width: 1,
                  //       ),
                  //     ),
                  //     child: const ListTile(
                  //       title: Text(
                  //         "Conflicts",
                  //       ),
                  //       trailing: Icon(Icons.arrow_forward_ios),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            children = <Widget>[
              const Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Error: ${snapshot.error}'),
              ),
            ];
          } else {
            children = <Widget>[
              SizedBox(
                width: 60,
                height: 60,
                child: CircularProgressIndicator(),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
                child: Text(AppLocalizations.of(context)?.waiting ?? ""),
              ),
            ];
          }
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: children,
            ),
          );
        },
      ),
    );
  }
}
