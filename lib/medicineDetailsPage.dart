import 'package:flutter/material.dart';

import 'addMedicinePage.dart';
import 'editMedicinePage.dart';
import 'medicine_listing.dart';
import 'app_State.dart';
import 'package:gtext/gtext.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MedicineDetailsScreen extends StatefulWidget {
  MedicineListing medicine;
  final bool isTaking;
  final int index;

  MedicineDetailsScreen({
    super.key,
    required this.medicine,
    required this.isTaking,
    required this.index,
  });

  @override
  State<MedicineDetailsScreen> createState() => _MedicineDetailsScreenState();
}

class _MedicineDetailsScreenState extends State<MedicineDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 80,
        backgroundColor: const Color.fromRGBO(226, 252, 214, 1.0),
        flexibleSpace: Center(
          child: Container(
            height: 65,
            width: 65,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/logo.png'), fit: BoxFit.fitHeight),
            ),
          ),
        ),
        actions: !widget.isTaking
            ? [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      IconButton(
                          icon: const Icon(Icons.add_box_rounded,
                              color: Color.fromRGBO(9, 93, 126, 1.0)),
                          onPressed: () {
                            // Navigate to the page where the user can add medicine details
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => AddMedicinePage(
                                              medicine: widget.medicine,
                                            )))
                                .then((value) => Navigator.pop(context));
                          }),
                      GText(
                        'Add Medicine',
                        toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",
                        style:
                            TextStyle(color: Color.fromRGBO(9, 93, 126, 1.0)),
                      )
                    ],
                  ),
                )
              ]
            : [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      IconButton(
                          icon: const Icon(Icons.edit,
                              color: Color.fromRGBO(9, 93, 126, 1.0)),
                          onPressed: () {
                            // Navigate to the page where the user can add medicine details
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EditMedicinePage(
                                          index: widget.index,
                                          medicine: widget.medicine,
                                        ))).then((returnedMedicine) {
                              // Handle the returned medicine details here
                              if (returnedMedicine != null) {
                                setState(() {
                                  widget.medicine = returnedMedicine;
                                });
                              }
                            });
                          }),
                      const Text(
                        'Edit Medicine',
                        style:
                            TextStyle(color: Color.fromRGBO(9, 93, 126, 1.0)),
                      )
                    ],
                  ),
                )
              ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: [
            GText(widget.medicine.brandName ?? "N/A",
                style: const TextStyle(
                    // fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                    fontSize: 25,
                    color: Color.fromRGBO(9, 93, 126, 1.0)), toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),
            const SizedBox(height: 8),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: GText(widget.isTaking
                  ? getSelectedDaysString(widget.medicine.frequency!)
                  : '', toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),
            ),
            GText('Purpose: ${widget.medicine.purpose ?? "N/A"}',
                style: const TextStyle(fontSize: 16), toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),
            const SizedBox(height: 8),
            GText('Warnings: ${widget.medicine.warnings?.join(", ") ?? "N/A"}',
                style: const TextStyle(fontSize: 16), toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),
            const SizedBox(height: 8),
            GText('Directions: ${widget.medicine.directions ?? "N/A"}',
                style: const TextStyle(fontSize: 16), toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),
            const SizedBox(height: 8),
            GText('Route: ${widget.medicine.route ?? "N/A"}',
                style: const TextStyle(fontSize: 16), toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),
            const SizedBox(height: 8),
            GText(
                'Ingredients: ${widget.medicine.ingredients?.join(", ") ?? "N/A"}',
                style: const TextStyle(fontSize: 16), toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),

          ],
        ),
      ),
    );
  }

  String getSelectedDaysString(List selectedDays) {
    List<String> daysOfWeek = [AppLocalizations.of(context)?.monday ?? "", AppLocalizations.of(context)?.tuesday ?? "", AppLocalizations.of(context)?.wednesday ?? "", AppLocalizations.of(context)?.thursday ?? "", AppLocalizations.of(context)?.friday ?? "", AppLocalizations.of(context)?.saturday ?? "", AppLocalizations.of(context)?.sunday ?? ""];

    List<String> selectedDaysList = [];

    for (int i = 0; i < selectedDays.length; i++) {
      if (selectedDays[i]) {
        selectedDaysList.add(daysOfWeek[i]);
      }
    }
    return selectedDaysList.join(', ');
  }
}
