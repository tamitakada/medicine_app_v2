import 'package:flutter/material.dart';
import 'user.dart';

import 'medicineDetailsPage.dart';

class MediciListProfile extends StatelessWidget {
  const MediciListProfile({super.key, required this.user});

  final UserStuff user;


  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: user.meds != null
            ? ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: user.meds!.length,
                itemBuilder: (BuildContext context, int index) {
                  print(user.meds![index].frequency);
                  return Card(
                    child: ListTile(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MedicineDetailsScreen(
                                  medicine: user.meds![index], isTaking: true,index: index )),
                        );
                      },
                      title: Text(user.meds![index].brandName!),
                      subtitle: Text(user.meds![index].time
                          .toString()
                          .substring(
                              1, user.meds![0].time.toString()!.length - 1)),
                      trailing: Icon(Icons.arrow_forward_ios_sharp),
                    ),
                  );
                })
            : const SizedBox());
  }
}
