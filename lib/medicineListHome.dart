import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'user.dart';

import 'database.dart';
import 'medicineDetailsPage.dart';
import 'medicine_listing.dart';
import 'package:gtext/gtext.dart';
import 'app_State.dart';

class MediciListHome extends StatelessWidget {
  const MediciListHome({
    super.key,
  });

  List<Widget> getMedicineWidgets(UserStuff user, context) {
    // Get the current day of the week (1 for Monday, 7 for Sunday)
    int currentDayOfWeek = DateTime.now().weekday;

    List<Widget> medicineWidgets = [];

    // Iterate over user's medicines
    for (int i = 0; i < user.meds!.length; i++) {
      MedicineListing medicine = user.meds![i];
      // Check if the medicine should be taken on the current day
      if (medicine.frequency![currentDayOfWeek - 1]) {
        // Sort the times in ascending order
        List<String> sortedTimes = List<String>.from(medicine.time!);
        sortedTimes.sort();

        // Iterate over each sorted time for the medicine
        for (String timeString in sortedTimes) {
          // Parse the time strings into DateTime objects
          DateTime currentTime = DateTime.now();
          DateTime medicineTime = _parseTime(timeString);
          // Check if the time has already passed
          if (currentTime.isBefore(medicineTime)) {
            // Add the medicine widget to the list
            medicineWidgets.add(
              Card(
                child: ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MedicineDetailsScreen(
                          medicine: medicine,
                          isTaking: true,
                          index: i,
                        ),
                      ),
                    );
                  },
                  title: GText(medicine.brandName!, toLang: App_State.locale.value == "zh" ? "zh-cn" : "en",),
                  subtitle: Text(
                    timeString,
                  ),
                  trailing: Icon(Icons.arrow_forward_ios_sharp),
                ),
              ),
            );
          }
        }
      }
    }

    return medicineWidgets;
  }

  DateTime _parseTime(String timeString) {
    // Parse the time string
    final format = DateFormat.jm(); // jm means "hour:minute am/pm"
    DateTime parsedTime = format.parse(timeString);

    // Get the current date
    DateTime currentDate = DateTime.now();

    // Combine the parsed time with the current date
    DateTime combinedDateTime = DateTime(
      currentDate.year,
      currentDate.month,
      currentDate.day,
      parsedTime.hour,
      parsedTime.minute,
    );

    return combinedDateTime;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: FutureBuilder<UserStuff>(
        future: Database.getUser(
          FirebaseAuth.instance.currentUser?.email ?? "Default Value",
        ),
        builder: (BuildContext context, AsyncSnapshot<UserStuff> snapshot) {
          if (snapshot.hasData) {
            UserStuff user = snapshot.data!;
            List<Widget> medicineWidgets = getMedicineWidgets(user, context);

            // Return the ListView with the medicine widgets
            return ListView(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              padding: const EdgeInsets.all(8),
              children: medicineWidgets,
            );
          } else {
            // Return an empty container or loading indicator when there's no data yet
            return const SizedBox();
          }
        },
      ),
    );
  }
}
