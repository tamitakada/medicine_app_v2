import 'package:shared_preferences/shared_preferences.dart';
import 'package:chat_gpt_sdk/chat_gpt_sdk.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'medicine_listing.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Translation{

  static var language = "Chinese";

  static Future<SharedPreferences> getPreferences() async {

    return await SharedPreferences.getInstance();
  }

  static Future<List<MedicineListing>> getMedsTranslation(List<MedicineListing> list) async {

    late final OpenAI _openAI;

    String message = "";

    for (MedicineListing m in list){

      message += (m.brandName ?? "") + "\n" + (m.purpose ?? "")  + "\n" + (m.warnings.toString()) + "\n"
          + (m.directions ?? "") + "\n" + (m.route ?? "") + "\n" + (m.ingredients.toString()) + "\n"
          + (m.frequency.toString()) + "\n" + (m.time.toString()) + "\n\n";
    }

    _openAI = OpenAI.instance.build(
      token: dotenv.env['OPENAI_API_KEY'],
      baseOption: HttpSetup(
        receiveTimeout: const Duration(seconds: 30),
      ),
    );

    String userPrompt = "Translate to Chinese: "
        + message + ". Please give me only the translation. Do not"
        " write anything before the translation";

    final request = ChatCompleteText(
      messages: [
        Messages(
          role: Role.user,
          content: userPrompt,
        ),
      ],
      maxToken: 1500,
      model: GptTurbo0631Model(),
    );

    print(userPrompt);

    ChatCTResponse? response = await _openAI.onChatCompletion(request: request);
    String str = response!.choices.first.message!.content.trim();
    List<String> medList = str.split("\n\n");
    List<MedicineListing> finalList = [];

    for(String s in medList){

      List<String> translatedText = str.split("\n");

      finalList.add(MedicineListing(
          brandName: translatedText[0],
          purpose: translatedText[1],
          warnings: translatedText[2].split(", "),
          directions: translatedText[3],
          route: translatedText[4],
          ingredients: translatedText[5].split(", "),
          frequency: translatedText[6].split(", "),
          time: translatedText[7].split(", ")
      ));
    }



    return finalList;
  }

  static Future<MedicineListing> getMedTranslation(MedicineListing med) async {

    late final OpenAI _openAI;

    String message = (med.brandName ?? "") + "\n" + (med.purpose ?? "")  + "\n" + (med.warnings.toString()) + "\n"
          + (med.directions ?? "") + "\n" + (med.route ?? "") + "\n" + (med.ingredients.toString()) + "\n"
          + (med.frequency.toString()) + "\n" + (med.time.toString()) + "\n\n";

    _openAI = OpenAI.instance.build(
      token: dotenv.env['OPENAI_API_KEY'],
      baseOption: HttpSetup(
        receiveTimeout: const Duration(seconds: 30),
      ),
    );

    String userPrompt = "Translate to Chinese: "
        + message + ". Please give me only the translation. Do not"
        " write anything before the translation";

    final request = ChatCompleteText(
      messages: [
        Messages(
          role: Role.user,
          content: userPrompt,
        ),
      ],
      maxToken: 2000,
      model: GptTurbo0631Model(),
    );

    ChatCTResponse? response = await _openAI.onChatCompletion(request: request);
    String str = response!.choices.first.message!.content.trim();
    print(str);
    List<String> translatedText = str.split("\n");

    return MedicineListing(
      brandName: translatedText.isNotEmpty ? translatedText[0] : "",
      purpose: translatedText.length > 1 ?  translatedText[1] : "",
      warnings: translatedText.length > 2 ? translatedText[2].split(", ") : [],
      directions: translatedText.length > 3 ? translatedText[3] : "",
      route: translatedText.length > 4 ? translatedText[4] : "",
      ingredients: translatedText.length > 5 ? translatedText[5].split(", ") : [],
      frequency: translatedText.length > 6 ? translatedText[6].split(", ") : [],
      time: translatedText.length > 7 ? translatedText[7].split(", ") : []
    );
  }

}