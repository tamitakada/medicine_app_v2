import 'package:flutter/material.dart';
import 'user.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Menu extends StatelessWidget {
  const Menu({super.key, required this.user});

  final UserStuff user;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: height * 0.4,
        width: width * 0.5,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            borderRadius: const BorderRadius.all(Radius.circular(20))),
        child: Column(
          children: [
            Text(AppLocalizations.of(context)?.information ?? ""),
            ListTile(
              leading: const Icon(Icons.person),
              title: Text(AppLocalizations.of(context)?.firstName ?? ""),
              trailing: Text(user.firstName),
            ),
            ListTile(
              leading: const Icon(Icons.person),
              title: Text(AppLocalizations.of(context)?.lastName ?? ""),
              trailing: Text(user.lastName),
            ),
            ListTile(
              leading: const Icon(Icons.e_mobiledata),
              title: Text(AppLocalizations.of(context)?.age ?? ""),
              trailing: Text("${user.age}"),
            ),
          ],
        ),
      ),
    );
  }
}
