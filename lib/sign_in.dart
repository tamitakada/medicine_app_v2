import 'package:flutter/material.dart';
import 'sign_up.dart';
import 'route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'translation.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: ListView(
        children: [
          const SignInForm(),
        ],
      ),
    );

  }
}

class SignInForm extends StatefulWidget {
  const SignInForm({super.key});

  @override
  State<SignInForm> createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  final formKey = GlobalKey<FormState>();

  TextEditingController passwordControl = TextEditingController();
  TextEditingController emailControl = TextEditingController();

  bool loginError = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              color: const Color.fromRGBO(226, 252, 214, 1.0),
              height: 250,
              width: width,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Image.asset(
                  'assets/logo.png',
                ),
              )),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Pillpedia',
              style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.email),
                labelText: AppLocalizations.of(context)?.email ?? "",
                border: OutlineInputBorder(),
              ),
              controller: emailControl,
              validator: (val) {
                if (val!.isEmpty) {
                  return AppLocalizations.of(context)?.emailError ?? "";
                }

                return null;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.password),
                labelText: AppLocalizations.of(context)?.password ?? "",
                border: OutlineInputBorder(),
              ),
              controller: passwordControl,
              validator: (val) {
                if (val!.isEmpty) {
                  //"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"

                  return AppLocalizations.of(context)?.passwordError ?? "";
                }

                return null;
              },
              obscureText: true,
            ),
          ),
          ElevatedButton(
            onPressed: () {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();
                getUser(emailControl.text, passwordControl.text).then((login) {
                  if (login) {
                    emailControl.text = '';
                    passwordControl.text = '';
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const RoutePage()));
                  } else {
                    setState(() {
                      loginError = true;
                    });
                  }
                });
              }
            },
            child: Text(AppLocalizations.of(context)?.signIn ?? ""),
          ),
          loginError ? Text(AppLocalizations.of(context)?.signInError ?? "") : Container(),
          Padding(
            padding: const EdgeInsets.only(
              left: 20,
              right: 30,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  child: Text(
                    AppLocalizations.of(context)?.register ?? "",
                    style: TextStyle(
                      color: Colors.lightBlue,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SignUpPage()));
                  },
                ),
                GestureDetector(
                  child: Text(
                    AppLocalizations.of(context)?.forgot ?? "",
                    style: TextStyle(
                      color: Colors.lightBlue,
                    ),
                  ),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> getUser(String email, String password) async {
    try {
      var user = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      print(e);
      return false;
    }

    return true;
  }
}
